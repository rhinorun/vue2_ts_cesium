import axios, { AxiosRequestConfig } from 'axios'
import VueAxios from 'vue-axios'
// import { Toast } from 'vant'
// import CityModule from '../store/module/CityModule'
const Loading = {
  // install 是默认的方法。当外界在 use 这个组件的时候，就会调用本身的 install 方法，同时传一个 Vue 这个类的参数。
  install (Vue:any) {
    // Vue.use(Toast)
    // 配置axios
    const httpConfig = axios.create({
      // baseURL: 'https://m.maizuo.com',
      timeout: 4000
    })
    // 添加请求拦截器
    httpConfig.interceptors.request.use((config: AxiosRequestConfig) => {
      // config.headers['X-Client-Info'] = `{"a":"3000","ch":"1002","v":"5.0.4","e":"16073369421366525449601025","bc":${CityModule.state.cityId}}`
      // // 在发送请求之前做些什么
      // // Toast.loading({
      // //   message: '加载中...',
      // //   // overlay: true,
      // //   forbidClick: true,
      // //   duration: 0
      // // })
      // config.baseURL = (config.url!.indexOf('/ajax/') !== -1) ? '' : 'https://m.maizuo.com'
      // console.log(config)
      return config
    }, error => {
      // 对请求错误做些什么
      // Toast.clear()
      return Promise.reject(error)
    })

    // 添加响应拦截器
    httpConfig.interceptors.response.use(response => {
      // 对响应数据做点什么
      // Toast.clear()
      return response
    }, error => {
      // 对响应错误做点什么
      // Toast.clear()
      return Promise.reject(error)
    })
    Vue.use(VueAxios, httpConfig)
  }
}

export default Loading
